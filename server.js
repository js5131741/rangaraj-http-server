const http = require("http");
const { v4: uuidv4 } = require("uuid");

const port = 3000;

const server = http.createServer((req, res) => {
  if (req.method === "GET") {
    if (req.url === "/html") {
      // Handle GET /html
      res.writeHead(200, { "Content-Type": "text/html" });
      res.end(`
        <!DOCTYPE html>
        <html>
          <head></head>
          <body>
            <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
            <p>- Martin Fowler</p>
          </body>
        </html>
      `);
    } else if (req.url === "/json") {
      // Handle GET /json
      res.writeHead(200, { "Content-Type": "application/json" });
      const jsonData = {
        slideshow: {
          author: "Yours Truly",
          date: "date of publication",
          slides: [
            {
              title: "Wake up to WonderWidgets!",
              type: "all",
            },
            {
              items: [
                "Why <em>WonderWidgets</em> are great",
                "Who <em>buys</em> WonderWidgets",
              ],
              title: "Overview",
              type: "all",
            },
          ],
          title: "Sample Slide Show",
        },
      };
      res.end(JSON.stringify(jsonData));
    } else if (req.url === "/uuid") {
      // Handle GET /uuid
      res.writeHead(200, { "Content-Type": "application/json" });
      const generatedUUID = uuidv4();
      res.end(JSON.stringify({ uuid: generatedUUID }));
    } else if (req.url.startsWith("/status/")) {
      // Handle GET /status/{status_code}
      const statusCode = parseInt(req.url.split("/").pop());
      res.writeHead(statusCode, { "Content-Type": "text/plain" });
      res.end(`Response with status code ${statusCode}`);
    } else if (req.url.startsWith("/delay/")) {
      // Handle GET /delay/{delay_in_seconds}
      const delayInSeconds = parseInt(req.url.split("/").pop());
      setTimeout(() => {
        res.writeHead(200, { "Content-Type": "text/plain" });
        res.end(`Response after ${delayInSeconds} seconds`);
      }, delayInSeconds * 1000);
    } else {
      // Handle unknown routes
      res.writeHead(404, { "Content-Type": "text/plain" });
      res.end("Not Found");
    }
  } else {
    // Handle unsupported HTTP methods
    res.writeHead(405, { "Content-Type": "text/plain" });
    res.end("Method Not Allowed");
  }
});

server.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}/`);
});
